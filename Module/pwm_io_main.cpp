#include <stdio.h>

#include "pico/stdlib.h"
#include "pico/time.h"

#include "pwm_in.h"

#include "pico/binary_info.h"


/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
 _    _ __          __   _____                __  _
 | |  | |\ \        / /  / ____|              / _|(_)
 | |__| | \ \  /\  / /  | |      ___   _ __  | |_  _   __ _
 |  __  |  \ \/  \/ /   | |     / _ \ | '_ \ |  _|| | / _` |
 | |  | |   \  /\  /    | |____| (_) || | | || |  | || (_| |
 |_|  |_|    \/  \/      \_____|\___/ |_| |_||_|  |_| \__, |
                                                       __/ |
                                                      |___/
-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/
#define NUMBER_OF_INPUT_PINS 4

uint input_pin_list[NUMBER_OF_INPUT_PINS] = {29,6,7,0};

/*-----------------------------------------------------------------------------------------------------------------------------------------------------------------
   _____  _         _             _  __      __           _         _      _
  / ____|| |       | |           | | \ \    / /          (_)       | |    | |
 | |  __ | |  ___  | |__    __ _ | |  \ \  / /__ _  _ __  _   __ _ | |__  | |  ___  ___
 | | |_ || | / _ \ | '_ \  / _` || |   \ \/ // _` || '__|| | / _` || '_ \ | | / _ \/ __|
 | |__| || || (_) || |_) || (_| || |    \  /| (_| || |   | || (_| || |_) || ||  __/\__ \
  \_____||_| \___/ |_.__/  \__,_||_|     \/  \__,_||_|   |_| \__,_||_.__/ |_| \___||___/
-----------------------------------------------------------------------------------------------------------------------------------------------------------------*/

float pulsewidth[NUMBER_OF_INPUT_PINS] = {0,0,0,0};
float period[NUMBER_OF_INPUT_PINS] = {0,0,0,0};
float duty_cycle[NUMBER_OF_INPUT_PINS] = {0,0,0,0};

bool print_data = false;

bool print_data_timer_callback(struct repeating_timer *t)
{
    print_data = true;
    return true;
}

int main()
{
    stdio_init_all();

    PwmIn pwmIn(input_pin_list, NUMBER_OF_INPUT_PINS);

    struct repeating_timer sensor_readout_timer;
    add_repeating_timer_ms(20, print_data_timer_callback, NULL, &sensor_readout_timer);
    
    while (true)
    {
        for (int idx = 0; idx < 4; idx++)
        {
            // Read the input values for each Channel
            float new_read_values[3];
            pwmIn.read_PWM(new_read_values, idx);
            if(new_read_values[0] > 0)
            {
                pulsewidth[idx] = new_read_values[0];
                period[idx] = new_read_values[1];
                duty_cycle[idx] = new_read_values[2];
            }
            
        }

        if(print_data)
        {
           for (int i = 0; i < 4; i++)
           {
            printf("%10.8f,", pulsewidth[i]); //Change what to print
           }
           printf("\n");
           print_data = false;
        }
    }
    
}
