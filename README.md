# PWM_IN_UART

## Project Setup for Windows
Follow the [Install Guide](https://www.raspberrypi.com/news/raspberry-pi-pico-windows-installer/) for setup instructions.

## Executables
This project includes two .uf2 files:

1. `./build/Module/pwm_io_main_dutyCycle.uf2`
2. `./build/Module/pwm_io_main_pulseWidth.uf2`

The only difference between the two executables is that the RP2040 either returns the duty cycle or the pulse width of the PWM input.

## Hardware Connection
Refer to the image below for hardware connection details:

![Hardware Connection](Images/Pins.png)